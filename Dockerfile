FROM openjdk:8-jdk-slim
RUN apt-get update; \
    apt-get install -y --no-install-recommends \
    fontconfig libfreetype6
ADD target/empresas-demo-0.0.1-SNAPSHOT.jar api.jar
EXPOSE 9090
ENTRYPOINT java -Xms256m -Xmx1536m -Djava.security.egd=file:/dev/./urandom -jar api.jar