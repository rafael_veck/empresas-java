# Desafio Pessoa Desenvolvedora Java

## 🏗 O que fazer?

- Você deve realizar um *fork* deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, **NÃO** é necessário criar um *Pull Request* para isso, nós iremos avaliar e retornar por e-mail o resultado do teste

# 🚨 Requisitos

- A API deve ser construída em Java (8 ou superior) utilizando Spring Framework (2.2 ou superior)
- Implementar autenticação seguindo o padrão ***JWT***, lembrando que o token a ser recebido deve estar no formado ***Bearer***
- Implementar operações no banco de dados utilizando ***Spring Data JPA*** & ***Hibernate***
- **Bancos relacionais permitidos**
    - *MySQL* (prioritariamente)
    - *PostgreSQL*
- As entidades deversão ser criadas como tabelas utilizando a ferramenta de migração **Flyway**. Portanto, os scripts de **migrations** para geração das tabelas devem ser enviados no teste
- Sua API deverá seguir os padrões REST na construção das rotas e retornos
- Sua API deverá conter documentação viva utilizando a *OpenAPI Specification* (**Swagger**)
- Caso haja alguma particularidade de implementação, instruções para execução do projeto deverão ser enviadas

# 🎁 Extra

- Testes unitários
- Teste de integração da API em linguagem de sua preferência (damos importância para pirâmide de testes)
- Cobertura de testes utilizando Sonarqube
- Utilização de *Docker* (enviar todos os arquivos e instruções necessárias para execução do projeto)

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do projeto
- Utilização de código limpo e princípios **SOLID**
- Segurança da API, como autenticação, senhas salvas no banco, *SQL Injection* e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção  [O que desenvolver?](##--o-que-desenvolver)

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deverá conter as seguintes funcionalidades:

- Administrador
    - Cadastro
    - Edição
    - Exclusão lógica (desativação)
    - Listagem de usuários não administradores ativos
        - Opção de trazer registros paginados
        - Retornar usuários por ordem alfabética
- Usuário
    - Cadastro
    - Edição
    - Exclusão lógica (desativação)
- Filmes
    - Cadastro (somente um usuário administrador poderá realizar esse cadastro)
    - Voto (a contagem de votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
    - Listagem
        - Opção de filtros por diretor, nome, gênero e/ou atores
        - Opção de trazer registros paginados
        - Retornar a lista ordenada por filmes mais votados e por ordem alfabética
    - Detalhes do filme trazendo todas as informações sobre o filme, inclusive a média dos votos

**Obs.:** 

**Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é um usuário administrador ou não.**

**Caso não consiga concluir todos os itens propostos, é importante que nos envie a implementação até onde foi possível para que possamos avaliar**

# Como testar?

Ao rodar a aplicação pela primeira vez, o usuário "admin" com senha "123456" será cadastrado automaticamente na base de dados via migration.

### Swagger
```
http://localhost:9090/swagger-ui.html
```

### Banco Mysql
```
docker run \
--name db_mysql \
-e MYSQL_ROOT_PASSWORD=demo \
-e MYSQL_DATABASE=demo \
-e MYSQL_USER=demo \
-e MYSQL_PASSWORD=demo \
-p 3306:3306 \
-d mysql:8.0.26
```

### Sonar
```
mvn sonar:sonar \
  -Dsonar.projectKey=br.tec.rvs:empresas-demo \
  -Dsonar.java.binaries=target/classes/ \
  -Dsonar.sources=src/main/ \
  -Dsonar.host.url=<SONARQUBE URL> \
  -Dsonar.login=<API KEY>
```

### Docker Build
```
docker build -f Dockerfile -t api-empresas-demo .
```

### Docker Run
```
docker run \
--name demo-api \
-p 9090:9090 \
-d api-empresas-demo
```

### Docker Net
```
docker network create demo-net
docker network connect demo-net db_mysql
docker network connect demo-net demo-api
```

### Exemplos de chamadas Rest

- Login
```
curl -X POST "http://localhost:9090/api/login/v1/login" \
-H "accept: */*" -H "Content-Type: application/json" \
-d "{ \"password\": \"123456\", \"username\": \"admin\"}"
```
- Administrador > Cadastro
````
curl -X POST "http://localhost:9090/api/v1/user" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg" -H "Content-Type: application/json" \
-d "{ \"enable\": true, \"password\": \"12345678\", \"role\": \"ROLE_ADMIN\", \"username\": \"josesilva\"}"
````
- Administrador > Edição
````
curl -X PUT "http://localhost:9090/api/v1/user/2" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg" -H "Content-Type: application/json" \
-d "{ \"enable\": true, \"password\": \"12345678\", \"role\": \"ROLE_ADMIN\", \"username\": \"jose_silva\"}"
````
- Administrador > Exclusão lógica (desativação)
```
curl -X DELETE "http://localhost:9090/api/v1/user?id=2" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Administrador > Listagem de usuários não administradores ativos > Opção de trazer registros paginados
```
curl -X GET "http://localhost:9090/api/v1/user?role=ROLE_USER?page=0" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Administrador > Listagem de usuários não administradores ativos > Retornar usuários por ordem alfabética
```
curl -X GET "http://localhost:9090/api/v1/user?sort=username,asc&role=ROLE_USER" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Usuário > Cadastro
```
curl -X POST "http://localhost:9090/api/v1/user" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg" \
-H "Content-Type: application/json" -d "{ \"enable\": true, \"password\": \"123123123\", \"role\": \"ROLE_USER\", \"username\": \"maria.santos\"}"
```
- Usuário > Edição
```
curl -X PUT "http://localhost:9090/api/v1/user/3" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg" \-H "Content-Type: application/json" \
-d "{ \"enable\": true, \"password\": \"123123123\", \"role\": \"ROLE_USER\", \"username\": \"maria.santos.silva\"}"
```
- Usuário > Exclusão lógica (desativação)
```
curl -X DELETE "http://localhost:9090/api/v1/user?id=3" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Filmes > Cadastro (somente um usuário administrador poderá realizar esse cadastro)
```
curl -X POST "http://localhost:9090/api/v1/movie" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg" -H "Content-Type: application/json" \
-d "{ \"actors\": [ {\"name\": \"Keanu Reeves\"}, {\"name\":\"Laurence Fishburne\"}, {\"name\":\"Carrie-Anne Moss\"} ], \"director\": \"As Wachowski\", \"gender\": \"ACAO\", \"name\": \"Matrix\"}"
```
- Filmes > Voto (a contagem de votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)
```
curl -X POST "http://localhost:9090/api/v1/movie/vote" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg" -H "Content-Type: application/json" \
-d "{ \"grade\": 4, \"movieId\": 1}"
```
- Filmes > Listagem > Opção de filtros por diretor, nome, gênero e/ou atores
```
curl -X GET "http://localhost:9090/api/v1/report/movie/all?sort=name,asc&size=20&page=0&actorName=lauren&directorName&movieGender=ACAO&movieName=Matrix" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Filmes > Listagem > Opção de trazer registros paginados
```
curl -X GET "http://localhost:9090/api/v1/report/movie/all?page=0" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Filmes > Listagem > Retornar a lista ordenada por filmes mais votados e por ordem alfabética
```
curl -X GET "http://localhost:9090/api/v1/report/movie/all?sort=qtdVotes,asc" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```
- Filmes > Detalhes do filme trazendo todas as informações sobre o filme, inclusive a média dos votos
```
curl -X GET "http://localhost:9090/api/v1/report/movie?id=1" \
-H "accept: */*" -H "Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxLGFkbWluIiwiaXNzIjoicnZzLnRlYy5iciIsImlhdCI6MTYyNzc3MDIxNSwiZXhwIjoxNjI4Mzc1MDE1fQ.aV10wNXgW7TkoxRwcsPi0tT495t7qWp_2Y-yFHQSTgUZC6ZLbetw_4uBsXDlA01S5FIycv9v_nbNi-jvQGszRg"
```

