CREATE VIEW movie_view AS
SELECT m.*,
(SELECT COUNT(*) FROM vote v WHERE v.id_movie = m.id) AS qtd_votes,
(SELECT SUM(grade)/COUNT(*) FROM vote v WHERE v.id_movie = m.id) AS average
FROM movie m