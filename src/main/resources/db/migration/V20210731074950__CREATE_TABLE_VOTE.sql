CREATE TABLE vote (
id BIGINT NOT NULL AUTO_INCREMENT,
id_user BIGINT NOT NULL,
id_movie BIGINT NOT NULL,
dt_register DATETIME NOT NULL,
grade SMALLINT NOT NULL,
PRIMARY KEY (id),
FOREIGN KEY (id_user) REFERENCES user(id),
FOREIGN KEY (id_movie) REFERENCES movie(id)
)