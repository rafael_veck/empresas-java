package br.tec.rvs.empresasdemo.enums;

public enum MovieGenderEnum {
    ACAO, TERROR, COMEDIA, DRAMA, SUSPENSE, ROMANCE
}
