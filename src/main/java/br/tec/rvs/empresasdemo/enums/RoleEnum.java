package br.tec.rvs.empresasdemo.enums;

public enum RoleEnum {
    ROLE_ADMIN, ROLE_USER
}
