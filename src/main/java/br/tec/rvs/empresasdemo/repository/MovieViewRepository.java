package br.tec.rvs.empresasdemo.repository;

import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;
import br.tec.rvs.empresasdemo.model.MovieView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieViewRepository extends JpaRepository<MovieView, Long> {

    @Query("SELECT DISTINCT mv FROM MovieView mv " +
            "LEFT JOIN mv.actors a " +
            "WHERE (:directorName IS NULL OR mv.director LIKE %:directorName%) " +
            "AND (:movieName IS NULL OR mv.name LIKE %:movieName%) " +
            "AND (:movieGender IS NULL OR mv.gender = :movieGender) " +
            "AND (:actorName IS NULL OR a.name LIKE %:actorName%)")
    Page<MovieView> findAll(String directorName,
                        String movieName,
                        MovieGenderEnum movieGender,
                        String actorName,
                        Pageable pageable);
}
