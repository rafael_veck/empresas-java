package br.tec.rvs.empresasdemo.repository;

import br.tec.rvs.empresasdemo.enums.RoleEnum;
import br.tec.rvs.empresasdemo.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("FROM User u " +
            "WHERE (:search IS NULL OR u.username LIKE %:search%) " +
            "AND (:enable IS NULL OR u.enable = :enable) " +
            "AND (:role IS NULL OR u.role = :role)")
    Page<User> findAll(RoleEnum role, Boolean enable, String search, Pageable pageable);
    Optional<User> findByUsername(String username);
}
