package br.tec.rvs.empresasdemo.repository;

import br.tec.rvs.empresasdemo.model.Vote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VoteRepository extends JpaRepository<Vote, Long> {}
