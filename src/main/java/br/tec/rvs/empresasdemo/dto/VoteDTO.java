package br.tec.rvs.empresasdemo.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;

public class VoteDTO {
    @NotNull(message = "Informe o movie_id")
    private Long movieId;

    @Min(0)
    @Max(value = 4, message = "Informe um grade de 0 a 4")
    private int grade;

    private List<String> errors;

    public VoteDTO(Long movieId) {
        this.movieId = movieId;
    }

    public VoteDTO(Long movieId, List<String> errors) {
        this.movieId = movieId;
        this.errors = errors;
    }

    public Long getMovieId() {
        return movieId;
    }

    public void setMovieId(Long movieId) {
        this.movieId = movieId;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
