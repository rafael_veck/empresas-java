package br.tec.rvs.empresasdemo.dto;

import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class MovieDTO {
    private Long id;

    @NotBlank(message="Informe o name")
    @Size(min=1, max=100, message="Informe o nome de 1 a 100 caracteres")
    private String name;

    @NotNull(message="Informe o gender")
    private MovieGenderEnum gender;

    @NotBlank(message="Informe o director")
    @Size(min=1, max=100, message="Informe o director de 1 a 100 caracteres")
    private String director;

    private List<ActorDTO> actors;

    private List<String> errors;

    public MovieDTO(){

    }

    public MovieDTO(MovieGenderEnum gender, List<String> errors) {
        this.gender = gender;
        this.errors = errors;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieGenderEnum getGender() {
        return gender;
    }

    public void setGender(MovieGenderEnum gender) {
        this.gender = gender;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<ActorDTO> getActors() {
        return actors;
    }

    public void setActors(List<ActorDTO> actors) {
        this.actors = actors;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
