package br.tec.rvs.empresasdemo.dto;

import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;

import java.util.List;

public class MovieViewDTO{
    private Long id;
    private String name;
    private MovieGenderEnum gender;
    private String director;
    private List<ActorDTO> actors;
    private Integer qtdVotes;
    private Double average;

    public MovieViewDTO() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieGenderEnum getGender() {
        return gender;
    }

    public void setGender(MovieGenderEnum gender) {
        this.gender = gender;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public List<ActorDTO> getActors() {
        return actors;
    }

    public void setActors(List<ActorDTO> actors) {
        this.actors = actors;
    }

    public Integer getQtdVotes() {
        return qtdVotes;
    }

    public void setQtdVotes(Integer qtdVotes) {
        this.qtdVotes = qtdVotes;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }
}
