package br.tec.rvs.empresasdemo.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ActorDTO {
    private Long id;

    @NotBlank(message="Informe o name do ator")
    @Size(min=1, max=100, message="Informe o nome do ator de 1 a 100 caracteres")
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
