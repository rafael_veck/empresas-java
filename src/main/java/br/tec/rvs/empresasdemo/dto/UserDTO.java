package br.tec.rvs.empresasdemo.dto;

import br.tec.rvs.empresasdemo.enums.RoleEnum;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDTO {
    @NotBlank(message="Informe o username")
    @Size(min=1, max=100, message="Informe o username de 1 a 100 caracteres")
    private String username;

    @NotBlank(message="Informe o password")
    @Size(min=8, max=16, message="Informe uma senha de 8 a 16 caracteres")
    private String password;

    @NotNull(message="Informe a role")
    private RoleEnum role;

    @NotNull(message="Informe o campo enable")
    private boolean enable;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }
}
