package br.tec.rvs.empresasdemo.model;

import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private MovieGenderEnum gender;

    private String director;

    private LocalDateTime dtRegister;

    @OneToMany(mappedBy = "movie", orphanRemoval = true, cascade = CascadeType.ALL)
    private List<Actor> actors;

    public Movie() {

    }

    public Movie(Long id, String name, MovieGenderEnum gender, String director, LocalDateTime dtRegister) {
        this.id = id;
        this.name = name;
        this.gender = gender;
        this.director = director;
        this.dtRegister = dtRegister;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieGenderEnum getGender() {
        return gender;
    }

    public void setGender(MovieGenderEnum gender) {
        this.gender = gender;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public LocalDateTime getDtRegister() {
        return dtRegister;
    }

    public void setDtRegister(LocalDateTime dtRegister) {
        this.dtRegister = dtRegister;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }
}
