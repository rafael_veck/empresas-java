package br.tec.rvs.empresasdemo.model;

import javax.persistence.*;

@Entity
@Table(name = "actor")
public class ActorView {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="id_movie")
    private MovieView movie;

    public ActorView() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieView getMovie() {
        return movie;
    }

    public void setMovie(MovieView movie) {
        this.movie = movie;
    }
}
