package br.tec.rvs.empresasdemo.model;

import br.tec.rvs.empresasdemo.enums.RoleEnum;
import com.google.common.collect.Lists;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
public class User implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String username;

    private String password;

    private boolean enable;

    @Enumerated(EnumType.STRING)
    private RoleEnum role;

    private LocalDateTime dtRegister;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public boolean getEnable(){
        return this.enable;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public LocalDateTime getDtRegister() {
        return dtRegister;
    }

    public void setDtRegister(LocalDateTime dtRegister) {
        this.dtRegister = dtRegister;
    }

    @Override
    public Collection<Role> getAuthorities() {
        return Lists.newArrayList(new Role(role));
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        //TODO not implemented yet
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        //TODO not implemented yet
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        //TODO not implemented yet
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enable;
    }
}
