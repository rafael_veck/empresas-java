package br.tec.rvs.empresasdemo.model;

import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;
import org.springframework.data.annotation.Immutable;

import javax.persistence.*;
import java.util.Set;

@Entity
@Immutable
public class MovieView {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private MovieGenderEnum gender;

    private String director;

    private Integer qtdVotes;
    private Double average;

    @OneToMany(mappedBy = "movie")
    private Set<ActorView> actors;

    private MovieView(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MovieGenderEnum getGender() {
        return gender;
    }

    public void setGender(MovieGenderEnum gender) {
        this.gender = gender;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public Integer getQtdVotes() {
        return qtdVotes;
    }

    public void setQtdVotes(Integer qtdVotes) {
        this.qtdVotes = qtdVotes;
    }

    public Double getAverage() {
        return average;
    }

    public void setAverage(Double average) {
        this.average = average;
    }

    public Set<ActorView> getActors() {
        return actors;
    }

    public void setActors(Set<ActorView> actors) {
        this.actors = actors;
    }
}
