package br.tec.rvs.empresasdemo.model;

import br.tec.rvs.empresasdemo.enums.RoleEnum;
import org.springframework.security.core.GrantedAuthority;

public class Role implements GrantedAuthority {
    public static final String USER = "ROLE_USER";
    public static final String ADMIN = "ROLE_ADMIN";

    private final String authority;

    public Role(RoleEnum authority) {
        this.authority = authority.toString();
    }

    @Override
    public String getAuthority() {
        return authority;
    }
}
