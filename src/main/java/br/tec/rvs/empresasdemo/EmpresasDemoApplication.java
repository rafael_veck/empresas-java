package br.tec.rvs.empresasdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpresasDemoApplication {
	public static void main(String[] args) {
		SpringApplication.run(EmpresasDemoApplication.class);
	}
}
