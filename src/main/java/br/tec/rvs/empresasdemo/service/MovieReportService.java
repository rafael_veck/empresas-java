package br.tec.rvs.empresasdemo.service;

import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;
import br.tec.rvs.empresasdemo.model.MovieView;
import br.tec.rvs.empresasdemo.repository.MovieViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MovieReportService {

    @Autowired
    private MovieViewRepository movieViewRepository;

    public Page<MovieView> findAll(String directorName,
                              String movieName,
                              MovieGenderEnum movieGender,
                              String actorName,
                              Pageable pageable){
        return movieViewRepository.findAll(directorName, movieName, movieGender, actorName, pageable);
    }

    public Optional<MovieView> findById(Long id){
        return movieViewRepository.findById(id);
    }
}
