package br.tec.rvs.empresasdemo.service;

import br.tec.rvs.empresasdemo.enums.RoleEnum;
import br.tec.rvs.empresasdemo.model.User;
import br.tec.rvs.empresasdemo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserService implements org.springframework.security.core.userdetails.UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    public Page<User> findAll(RoleEnum role, Boolean enable, String search, Pageable pageable){
        return userRepository.findAll(role, enable, search, pageable);
    }

    public User save(User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setId(null);
        user.setPassword(encoder.encode(user.getPassword()));
        user.setDtRegister(LocalDateTime.now());
        return userRepository.save(user);
    }

    public User update(User user){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        user.setPassword(encoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public void delete(User user){
        user.setEnable(false);
        userRepository.save(user);
    }

    public Optional<User> findById(Long id){
        return userRepository.findById(id);
    }

    public Optional<User> findByUsername(String username){
        return userRepository.findByUsername(username);
    }

    @Override
    public UserDetails loadUserByUsername(String username) {
        return findByUsername(username)
                .orElseThrow(
                        () -> new UsernameNotFoundException(
                                String.format("User with username - %s, not found", username))
                );
    }
}
