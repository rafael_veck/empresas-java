package br.tec.rvs.empresasdemo.service;

import br.tec.rvs.empresasdemo.model.Movie;
import br.tec.rvs.empresasdemo.model.MovieView;
import br.tec.rvs.empresasdemo.model.User;
import br.tec.rvs.empresasdemo.model.Vote;
import br.tec.rvs.empresasdemo.repository.MovieRepository;
import br.tec.rvs.empresasdemo.repository.MovieViewRepository;
import br.tec.rvs.empresasdemo.repository.VoteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class MovieService {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieViewRepository movieViewRepository;

    @Autowired
    private VoteRepository voteRepository;

    public Movie save(Movie movie){
        movie.setDtRegister(LocalDateTime.now());
        return movieRepository.save(movie);
    }

    public void vote(User user, Movie movie, int grade){
        Vote vote = new Vote(user, movie, grade);
        vote.setDtRegister(LocalDateTime.now());
        voteRepository.save(vote);
    }

    public Optional<Movie> findById(Long id){
        return movieRepository.findById(id);
    }

    public Optional<MovieView> findMovieViewById(Long id){
        return movieViewRepository.findById(id);
    }
}
