package br.tec.rvs.empresasdemo.controller;

import br.tec.rvs.empresasdemo.dto.MovieDTO;
import br.tec.rvs.empresasdemo.dto.VoteDTO;
import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;
import br.tec.rvs.empresasdemo.model.Actor;
import br.tec.rvs.empresasdemo.model.Movie;
import br.tec.rvs.empresasdemo.model.Role;
import br.tec.rvs.empresasdemo.model.User;
import br.tec.rvs.empresasdemo.service.MovieService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/")
@RolesAllowed({Role.ADMIN, Role.USER})
public class MovieController {

    @Autowired
    private MovieService movieService;

    @Autowired
    private ModelMapper modelMapper;

    @RolesAllowed(Role.ADMIN)
    @PostMapping("/v1/movie")
    @ApiOperation(value = "Cadastro de filme")
    public ResponseEntity<MovieDTO> save(@RequestBody @Valid MovieDTO request, BindingResult result){
        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());

            MovieDTO errorResponse = new MovieDTO(MovieGenderEnum.ACAO, errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        Movie saved = movieService.save(convertToEntity(request));
        MovieDTO response = convertToDto(saved);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PostMapping("/v1/movie/vote")
    @ApiOperation(value = "Voto em um filme")
    public ResponseEntity<VoteDTO> vote(@RequestBody @Valid VoteDTO voteDTO, BindingResult result){
        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors()
                    .stream()
                    .map(DefaultMessageSourceResolvable::getDefaultMessage)
                    .collect(Collectors.toList());

            VoteDTO errorResponse = new VoteDTO(0L, errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Optional<Movie> movie = movieService.findById(voteDTO.getMovieId());
        if (movie.isPresent()) {
            movieService.vote(user, movie.get(), voteDTO.getGrade());
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new VoteDTO(0L, Collections.singletonList("Movie not found")));
    }

    private MovieDTO convertToDto(Movie movie) {
        return modelMapper.map(movie, MovieDTO.class);
    }

    private Movie convertToEntity(MovieDTO movieDTO) {
        Long movieId = null;
        if (movieDTO.getId() != null && movieDTO.getId() > 0) {
            movieId = movieDTO.getId();
        }
        Movie movie = new Movie(
                movieId,
                movieDTO.getName(),
                movieDTO.getGender(),
                movieDTO.getDirector(),
                LocalDateTime.now());
        if (movieDTO.getActors() != null){
            List<Actor> actors = movieDTO.getActors().stream().filter(it -> it.getName() != null).map(a -> {
                Long id = null;
                if (a.getId() != null && a.getId() > 0) {
                    id = a.getId();
                }
                return new Actor(
                        id,
                        movie,
                        a.getName());
            }).collect(Collectors.toList());
            movie.setActors(actors);
        }
        return movie;
    }
}
