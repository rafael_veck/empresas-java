package br.tec.rvs.empresasdemo.controller;

import br.tec.rvs.empresasdemo.controller.response.UserResponse;
import br.tec.rvs.empresasdemo.dto.MovieViewDTO;
import br.tec.rvs.empresasdemo.enums.MovieGenderEnum;
import br.tec.rvs.empresasdemo.model.MovieView;
import br.tec.rvs.empresasdemo.model.Role;
import br.tec.rvs.empresasdemo.service.MovieReportService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/")
@RolesAllowed({Role.ADMIN, Role.USER})
public class MovieReportController {

    @Autowired
    private MovieReportService movieReportService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/v1/report/movie/all")
    @ApiOperation(value = "Consulta dos filmes cadastrados")
    public ResponseEntity<Page<MovieViewDTO>> findAll(
            @RequestParam(required = false) String directorName,
            @RequestParam(required = false) String movieName,
            @RequestParam(required = false) MovieGenderEnum movieGender,
            @RequestParam(required = false) String actorName,
            Pageable pageable){
        Page<MovieView> movies = movieReportService.findAll(directorName, movieName, movieGender, actorName, pageable);
        List<MovieViewDTO> content = movies.get().map(this::convertToDto).collect(Collectors.toList());
        Page<MovieViewDTO> response = new PageImpl(content, pageable, movies.getTotalElements());
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/v1/report/movie")
    @ApiOperation(value = "Consulta detalhes filme")
    public ResponseEntity<MovieViewDTO> findById(@RequestParam(required = true) Long id){
        Optional<MovieView> movie = movieReportService.findById(id);
        if (movie.isPresent()){
            return ResponseEntity.ok().body(convertToDto(movie.get()));
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    private MovieViewDTO convertToDto(MovieView movie) {
        return modelMapper.map(movie, MovieViewDTO.class);
    }
}
