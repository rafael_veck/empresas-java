package br.tec.rvs.empresasdemo.controller;

import br.tec.rvs.empresasdemo.controller.response.UserResponse;
import br.tec.rvs.empresasdemo.dto.UserDTO;
import br.tec.rvs.empresasdemo.enums.RoleEnum;
import br.tec.rvs.empresasdemo.model.Role;
import br.tec.rvs.empresasdemo.model.User;
import br.tec.rvs.empresasdemo.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RolesAllowed(Role.ADMIN)
@RequestMapping("api/")
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping("/v1/user")
    @ApiOperation(value = "Consulta dos usuarios cadastrados")
    public ResponseEntity<Page<UserResponse>> findAll(
            @RequestParam(required = false) RoleEnum role,
            @RequestParam(required = false) Boolean enable,
            @RequestParam(required = false) String search,
            Pageable pageable){
        Page<User> users = userService.findAll(role, enable, search, pageable);
        List<UserResponse> content = users.get().map(this::convertToUserResponse).collect(Collectors.toList());
        Page<UserResponse> response = new PageImpl(content, pageable, users.getTotalElements());
        return ResponseEntity.ok().body(response);
    }

    @PostMapping("/v1/user")
    @ApiOperation(value = "Cadastro de usuario")
    public ResponseEntity<UserResponse> save(@RequestBody @Valid UserDTO request, BindingResult result){
        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            UserResponse errorResponse = new UserResponse(errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        User saved = userService.save(convertToEntity(request));
        UserResponse response = convertToUserResponse(saved);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PutMapping("/v1/user/{id}")
    @ApiOperation(value = "Atualização de usuario")
    public ResponseEntity<UserResponse> update(@PathVariable("id") @NotNull @Min(1) Long id,
            @RequestBody @Valid UserDTO request, BindingResult result){
        if (result.hasErrors()){
            List<String> errors = result.getFieldErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.toList());
            UserResponse errorResponse = new UserResponse(errors);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
        Optional<User> user = userService.findById(id);
        if (!user.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        user.get().setEnable(request.getEnable());
        user.get().setUsername(request.getUsername());
        user.get().setRole(request.getRole());
        user.get().setPassword(request.getPassword());
        User saved = userService.update(user.get());
        UserResponse response = convertToUserResponse(saved);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping("/v1/user")
    @ApiOperation(value = "Exclusão de usuario")
    public ResponseEntity<Void> delete(@RequestParam("id") Long id){
        Optional<User> user = userService.findById(id);
        if (!user.isPresent()){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        userService.delete(user.get());
        return ResponseEntity.ok().build();
    }

    private UserResponse convertToUserResponse(User user) {
        return modelMapper.map(user, UserResponse.class);
    }

    private User convertToEntity(UserDTO userDTO) {
        return modelMapper.map(userDTO, User.class);
    }
}
