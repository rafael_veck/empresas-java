package br.tec.rvs.empresasdemo.controller.response;

import br.tec.rvs.empresasdemo.enums.RoleEnum;

import java.util.List;

public class UserResponse {
    private Long id;
    private String username;
    private RoleEnum role;
    private boolean enable;
    private List<String> errors;

    public UserResponse(List<String> errors) {
        this.errors = errors;
    }

    public UserResponse() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public RoleEnum getRole() {
        return role;
    }

    public void setRole(RoleEnum role) {
        this.role = role;
    }

    public boolean getEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }
}
